﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventClassExample
{
    public delegate void Callback(string s);
    public event Callback onSaluto;

    string prefix;

    public EventClassExample(string s)
    {
        prefix = s;
    }

    public void Saluta()
    {
        onSaluto(prefix);
    }
}
