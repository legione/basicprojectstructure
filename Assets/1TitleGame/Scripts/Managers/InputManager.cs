﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    // public delegate void PauseMenuDelegate();
    // PauseMenuDelegate onPauseMenuCallback;


    // public void Init(PauseMenuDelegate openMenu)
    // {
    //     onPauseMenuCallback = openMenu;
    // }

    UIManager uIManager;
    CubeSpawner cubeSpawner;

    public void Init(UIManager uiManager, CubeSpawner cubeSpawner)
    {
        uIManager = uiManager;
        this.cubeSpawner = cubeSpawner;


    }

    void Update()
    {
        if (Input.GetButtonDown("pauseMenu"))
        {
            uIManager.TogglePauseMenu();
        }

        if (Input.GetButtonDown("spawnObject"))
        {
            cubeSpawner.SpawnCube();
        }

    }
}
