﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainMenu : MonoBehaviour
{
    public ConfirmPopup quitPopup;
    public Button quitButton;

    void Awake()
    {
        quitButton.onClick.AddListener(OpenQuitPopup);
    }

    void OpenQuitPopup()
    {
        quitPopup.OpenPopUp(QuitGame);
    }

    void QuitGame()
    {
        Application.Quit();
        Debug.Log("quit game");
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
