﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class ConfirmPopup : MonoBehaviour
{
    event Action onConfirm;


    public Button confirmButton;

    public Button cancelButton;

    CanvasGroup canvasGroup;

    public void Init()
    {

    }

    public void OpenPopUp(Action onConfirmCallback)
    {
        onConfirm = onConfirmCallback;
        canvasGroup.alpha = 1;
    }

    public void ClosePopUp()
    {
        canvasGroup.alpha = 0;
    }

    void Confirm()
    {
        onConfirm();
        ClosePopUp();
    }

    void Cancel()
    {
        ClosePopUp();
    }

    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();

        cancelButton.onClick.AddListener(Cancel);
        confirmButton.onClick.AddListener(Confirm);
    }


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
