﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;


public static class SerializationUtils
{
    public static string SerializeDataToString<T>(T data)
    {
        XmlSerializer serializer = new XmlSerializer(data.GetType());
        using (StringWriter sw = new StringWriter())
        {
            serializer.Serialize(sw, data);
            return sw.ToString();
        }
    }

    public static T DeserializeStringToData<T>(string serializedData)
    {
        // Debug.Log("xml story: " + serializedData);
        XmlSerializer deserializer = new XmlSerializer(typeof(T));
        using (TextReader tr = new StringReader(serializedData))
        {
            return (T)deserializer.Deserialize(tr);
        }
    }

}
