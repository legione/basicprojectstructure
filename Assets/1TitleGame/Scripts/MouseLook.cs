﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{

    public float speedH = 2.0f;
    public float speedV = 2.0f;

    float yaw = 0.0f;
    float pitch = 0.0f;

    // Use this for initialization
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        yaw = transform.rotation.eulerAngles.y;
        pitch = transform.rotation.eulerAngles.x;
    }

    // Update is called once per frame
    void Update()
    {
        float Xaxis = Input.GetAxis("Mouse X");
        // Xaxis = Xaxis == 0 ? Input.GetAxis("Look X") : Xaxis;

        float Yaxis = Input.GetAxis("Mouse Y");
        // Yaxis = Yaxis == 0 ? Input.GetAxis("Look Y") : Yaxis;

        yaw += speedH * Xaxis;
        pitch -= speedV * Yaxis;

        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
    }
}
