﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UIManager))]
[RequireComponent(typeof(InputManager))]
public class GameBootstrapper : MonoBehaviour
{

    public CubeSpawner cubeSpawner;

    UIManager uiManager;
    InputManager inputManager;


    void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        uiManager = GetComponent<UIManager>();
        inputManager = GetComponent<InputManager>();

        cubeSpawner.Init();
        inputManager.Init(uiManager, cubeSpawner);



        // EventClassExample.onSaluto += (s) => { PrintString(s,"pippo"); };
        // EventClassExample.onSaluto += (s) => { PrintString(s,"pluto"); };

        // EventClassExample saluto1 = new EventClassExample("ciao");
        // saluto1.onSaluto += (s) => { PrintString(s, "pippo"); };

        // EventClassExample saluto2 = new EventClassExample("buongiorno");
        // saluto2.onSaluto += (s) => { PrintString(s, "pluto"); };

        // saluto1.Saluta();
        // saluto2.Saluta();

    }

    // Update is called once per frame
    void Update()
    {

    }

    void PrintString(string s, string s2)
    {
        Debug.Log(s + " " + s2);
    }

    void PrintString(string s)
    {
        Debug.Log(s + " test");
    }

    void PrintInt(int n, string intro)
    {
        Debug.Log(intro + n);
    }

    List<string> CreateList()
    {
        List<string> list = new List<string>();

        for (int i = 0; i <= 100; i++)
        {
            string item = "item_" + i;
            list.Add(item);
        }

        return list;
    }

    void PrintList(List<string> stringList)
    {
        foreach (string s in stringList)
        {
            Debug.Log(s);
        }
    }
}
