﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour
{
    readonly string saveKey = "cubesNumber";

    public GameObject cube;

    int cubesNumber;

    public void Init()
    {
        // DataSaver.SaveNumber(saveKey, 0);
        // DataSaver.ResetAll();

        cubesNumber = DataSaver.LoadNumber(saveKey);
        Debug.Log("Cubes number is: " + cubesNumber);

        StartCoroutine("Populate");
    }

    public void SpawnCube()
    {
        CreateCube();

        cubesNumber++;
        Debug.Log("Increased number of cubes: " + cubesNumber);

        DataSaver.SaveNumber(saveKey, cubesNumber);
    }

    void CreateCube()
    {
        GameObject cubeItem = Instantiate(cube, transform.position, transform.rotation);
    }

    IEnumerator Populate()
    {
        Debug.Log("Populating...");

        for (int i = 0; i < cubesNumber; i++)
        {
            yield return new WaitForSeconds(0.2f);
            CreateCube();
        }
    }
}
