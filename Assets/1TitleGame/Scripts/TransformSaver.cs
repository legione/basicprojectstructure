﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformSaver : MonoBehaviour
{
    readonly string transformSaveKey = "cameraTransform";

    // Se la rotazione non viene caricata e settata in awake viene sovrascritta dallo script di mouselook
    void Awake()
    {
        Quaternion q = DataSaver.LoadQuaternion(transformSaveKey);
        Debug.Log("Quaternion is: " + q);

        transform.rotation = q;
        Debug.Log("loaded rotation is: " + transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("saveTransform"))
        {
            Debug.Log("saving transform");
            SaveQuaternion();
        }
    }

    void SaveQuaternion()
    {
        Debug.Log("Quaternion is: " + transform.rotation);

        DataSaver.SaveQuaternion(transformSaveKey, transform.rotation);
    }
}
