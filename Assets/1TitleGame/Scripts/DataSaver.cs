﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataSaver
{


    public static void SaveNumber(string key, int n)
    {
        PlayerPrefs.SetInt(key, n);
    }

    public static int LoadNumber(string key)
    {
        return PlayerPrefs.GetInt(key);
    }

    public static void ResetAll()
    {
        PlayerPrefs.DeleteAll();
    }

    public static void SaveQuaternion(string key, Quaternion q)
    {
        SaveObject<Quaternion>(key, q);
    }

    public static Quaternion LoadQuaternion(string key)
    {
        return LoadObject<Quaternion>(key);
    }

    public static void SavePosition(string key, Vector3 pos)
    {
        SaveObject<Vector3>(key, pos);
    }

    public static Vector3 LoadPosition(string key)
    {
        return LoadObject<Vector3>(key);
    }


    static void SaveObject<T>(string key, T obj)
    {
        string serializedData = SerializationUtils.SerializeDataToString<T>(obj);

        PlayerPrefs.SetString(key, serializedData);
        // Debug.Log("rotation: " + serializedData);
        PlayerPrefs.Save();
    }

    static T LoadObject<T>(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            string serializedData = PlayerPrefs.GetString(key);
            // Debug.Log(serializedData);
            return SerializationUtils.DeserializeStringToData<T>(serializedData);
        }

        return default(T);
    }
}
